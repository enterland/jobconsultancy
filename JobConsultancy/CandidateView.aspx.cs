﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace JobConsultancy
{
    public partial class CandidateView : System.Web.UI.Page
    {
        myJobDataClassesDataContext mdc;
        protected void Page_Load(object sender, EventArgs e)
        {
            mdc = new myJobDataClassesDataContext();
            if (!IsPostBack)
            {
                ColData();
                
            }
          
        }
        private void getDataByUser()
        {

            var st = from l in mdc.students where l.eMail==Session["uid"]
                     select l;

            DataTable DTCol = new DataTable();

            DTCol.Columns.Add(new DataColumn("nam", Type.GetType("System.String")));
            DTCol.Columns.Add(new DataColumn("ph", Type.GetType("System.String")));
            DTCol.Columns.Add(new DataColumn("vw", Type.GetType("System.String")));
            DTCol.Columns.Add(new DataColumn("edt", Type.GetType("System.String")));


            DataRow dr;

            foreach (student s in st)
            {
                dr = DTCol.NewRow();

                dr[0] = s.stdName;
                dr[1] = s.stdPh;
                dr[2] = "CandidateDetail.aspx?ph=" + s.stdPh;
                dr[3] = "CandidateEdit.aspx?ph=" + s.stdPh;

                DTCol.Rows.Add(dr);
            }
            DataList1.DataSource = DTCol;
            DataList1.DataBind();
        }
        private void getData()
        {

            var st = from l in mdc.students
                     select l;

            DataTable DTCol = new DataTable();

            DTCol.Columns.Add(new DataColumn("nam", Type.GetType("System.String")));
            DTCol.Columns.Add(new DataColumn("ph", Type.GetType("System.String")));
            DTCol.Columns.Add(new DataColumn("vw", Type.GetType("System.String")));
            DTCol.Columns.Add(new DataColumn("edt", Type.GetType("System.String")));


            DataRow dr;

            foreach (student s in st)
            {
                dr = DTCol.NewRow();

                dr[0] = s.stdName;
                dr[1] = s.stdPh;
                dr[2] = "CandidateDetail.aspx?ph=" + s.stdPh;
                dr[3] = "CandidateEdit.aspx?ph=" + s.stdPh;


                DTCol.Rows.Add(dr);
            }
            DataList1.DataSource = DTCol;
            DataList1.DataBind();
        }

        public void ColData()
        {
            // string Uid = Request.QueryString["unid"];
            mdc = new myJobDataClassesDataContext();
            if (Session["uid"] == null)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "User Error !!", "alert('Required User Login')", true);

            }
            else
            {
                var u = mdc.userControls.Where(m => m.uid == Session["uid"]).FirstOrDefault();

                if (u.authType == 1)
                    getData();
                else
                    getDataByUser();
            }
            }

        
    }
}