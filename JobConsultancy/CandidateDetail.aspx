﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CandidateDetail.aspx.cs" Inherits="JobConsultancy.CandidateDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">

        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 270px;
        }
        .style4
        {
            width: 22px;
        }
        .style5
    {
        height: 58px;
        }
    .style6
    {
        width: 268px
    }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table class="style1">
    <tr>
        <td>
            <table class="style1">
                <tr>
                    <td align="center" class="style5" 
                            
                        
                        style="font-family: Andalus; font-size: xx-large; text-decoration: underline;" 
                        colspan="2">
&nbsp;Student Details</td>
                </tr>
                <tr>
                    <td class="style6">
                        &nbsp;</td>
                    <td>
                        <table class="style1">
                            <tr>
                                <td align="right" class="style3">
                                        Student Contact No.</td>
                                <td align="center" class="style4">
                                        :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtStdPh" runat="server" Width="240px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="style3">
                                        Student Name</td>
                                <td align="center" class="style4">
                                        :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtStdNam" runat="server" Width="240px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="style3">
                                        Sex</td>
                                <td align="center" class="style4">
                                        :</td>
                                <td align="left">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                                            RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True">Male</asp:ListItem>
                                        <asp:ListItem>Female</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="style3">
                                        Date Of Birth</td>
                                <td align="center" class="style4">
                                        :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtDob" runat="server" Width="240px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="style3">
                                        E_Mail</td>
                                <td align="center" class="style4">
                                        :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmail" runat="server" Width="240px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                            Academic Qualification :<br />
                        <br />
                        <asp:GridView ID="GridView3" runat="server" BackColor="#DEBA84" 
                            BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                            CellSpacing="2" Width="459px">
                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FFF1D4" />
                            <SortedAscendingHeaderStyle BackColor="#B95C30" />
                            <SortedDescendingCellStyle BackColor="#F1E5CE" />
                            <SortedDescendingHeaderStyle BackColor="#93451F" />
                        </asp:GridView>
                        <br />Extar Qualification / Expriences / Training :<br />
                        <asp:GridView ID="GridView4" runat="server" BackColor="LightGoldenrodYellow" 
                            BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" 
                            GridLines="None" Width="461px">
                            <AlternatingRowStyle BackColor="PaleGoldenrod" />
                            <FooterStyle BackColor="Tan" />
                            <HeaderStyle BackColor="Tan" Font-Bold="True" />
                            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" 
                                HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                            <SortedAscendingCellStyle BackColor="#FAFAE7" />
                            <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                            <SortedDescendingCellStyle BackColor="#E1DB9C" />
                            <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                        </asp:GridView>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="center" class="style6">
                            &nbsp;</td>
                    <td align="center">
                            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton3" runat="server" 
                            onclick="LinkButton3_Click">Download Resume</asp:LinkButton>
                        &nbsp;&nbsp;
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        &nbsp;</td>
                    <td>
                        <asp:Label ID="labDisp" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Content>
