﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CouresHotel.aspx.cs" Inherits="JobConsultancy.CouresHotel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name">Course: <span>Hotel Management</span></div></div>
            
          </div>
        </div>
      </div>
      <div class="container marg100">
        <div class="row">
          <div class="col-lg-12">
            <div class="promo-block">
              <div class="promo-text">Hotel Management Courses</div>
              <div class="center-line"></div>
            </div>
            <div class="marg50">
               
              <div class="row">
              <div class="col-lg-6">
			  <img src="dir/careergraph%20institute%20of%20professionals_3.jpg"height="250px" width="550px" alt="">
                  <h4><strong>Certification in Hotel Management</strong></h4>
                   <p align="left">
<strong>Duration:</strong> 180 classes<br>
<strong>Qualification:</strong> Matriculation<br>
<strong>Age: </strong>: 16 years to 27 years<br><br>
 <a href="enrol/enrol.html"><input type="submit" id="submit_btn" class="btn btn-default" value="Apply Now" /></a>
                   </p><hr> 
                </div>
                <div class="col-lg-6">
				<img src="dir/careergraph%20institute%20of%20professionals_4.jpg"height="250px" width="550px" alt="">
                 <h4><strong>Diploma in Hotel Management</strong></h4>
                   <p align="left">
<strong>Duration:</strong> 360 classes<br>
<strong>Qualification:</strong> 10+2 and Cirtification Hotel Management<br>
<strong>Age: </strong>18 years to 27 years<br><br>
 <a href="enrol/enrol.html"><input type="submit" id="submit_btn" class="btn btn-default" value="Apply Now" /></a>
                   </p><hr> 
                </div>
               </div>
             
             </div>
          </div>
        </div>
      </div>
</asp:Content>
