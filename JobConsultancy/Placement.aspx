﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Placement.aspx.cs" Inherits="JobConsultancy.Placement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="assets/js/sticky.js"></script>
    <script type="text/javascript" src="assets/js/jquery.tweet.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/js/retina.js"></script>
    <script type="text/javascript" src="assets/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="assets/js/portfolio-3.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dlmenu.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
<div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name">Success <span>Placement</span></div></div>
                      </div>
        </div>
      </div>
    
      <div class="container marg50">
        <div class="grid hover-3">
          <div class="cbp-l-grid-projects" id="grid-container-portfolio">
            <ul>
              <%--<li class="cbp-item wordpress design html">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p1.jpg" alt="">
                    
                  </figure> 
                </div>
              </li>
              <li class="cbp-item logo html graphic">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p2.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
              <li class="cbp-item logo graphic">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p3.jpg" alt="">
                    
                  </figure> 
                </div>
              </li>
              <li class="cbp-item design html wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p4.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
              <li class="cbp-item logo">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p5.jpg" alt="">
                                      </figure> 
                </div>
              </li>
              <li class="cbp-item design wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p6.jpg" alt="">
                     
                  </figure> 
                </div>
              </li>
              <li class="cbp-item logo graphic">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p7.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
              <li class="cbp-item html">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p8.jpg" alt="">
                    
                  </figure> 
                </div>
              </li>
              <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p9.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			  <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p10.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			  
			  <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p11.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			  <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p12.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			    <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/p13.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/14.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/15.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/16.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/17.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/18.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/19.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/21.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/22.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			  
			  <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/23.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/24.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/25.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			  <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/26.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/27.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/28.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			  
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/29.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/30.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/31.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/32.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/33.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/34.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/35.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/36.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/37.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/38.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/39.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/40.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/41.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>
			   <li class="cbp-item wordpress">
                <div class="portfolio-main">
                  <figure>
                    <img src="event/42.jpg" alt="">
                   
                  </figure> 
                </div>
              </li>--%>
			  
			  
			  
			  
            </ul>
          </div>
          <div class="col-lg-12">
           <!-- <div class="button-center"><a href="portfolio/loadmore-portfolio.html" class="btn-simple cbp-l-loadMore-button-link">Load Full Portfolio</a></div>-->
          </div>
        </div>  
      </div>
      <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="assets/js/sticky.js"></script>
    <script type="text/javascript" src="assets/js/jquery.tweet.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/js/retina.js"></script>
    <script type="text/javascript" src="assets/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="assets/js/portfolio-3.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dlmenu.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
</asp:Content>
