﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="JobConsultancy.ContactUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 103px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<meta content="xenia - responsive and retina ready template" name="description">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
    <link href="assets/images/favicon.html" rel="shortcut icon"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144x144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-precomposed.png" />
    <!-- JS FILES -->
    <script type="text/javascript" src="assets/js/jquery-1.20.2.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
    <!-- CSS FILES -->
    <link href="assets/css/style.css" media="screen" rel="stylesheet" type="text/css">
    <link href="assets/css/responsive.css" media="screen" rel="stylesheet" type="text/css">
    <link href="assets/css/flexslider.css" media="screen" rel="stylesheet" type="text/css">
    <link href="#" media="screen" rel="stylesheet" type="text/css" id="style-schem-color"> 
    <link href="#" media="screen" rel="stylesheet" type="text/css" id="style-schem"> 
<div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name">Contact <span>US</span></div></div>
             
          </div>
        </div>
      </div>
      <div class="container marg75">
        <div class="row">
          <div class="col-lg-12">
            <div class="promo-block">
              <div class="promo-text">Where Find Us?</div>
              <div class="center-line"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="container marg50">
        <div class="row">
          <div class="col-lg-12">
             
<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyASuwPKBouUdWZvZEaN59-ok-JryBq4GIs
    &q=LIFE+CAREER+SERVICES,REJINAGAR"
 width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div> 
        </div>
      </div>  <hr>
      <div class="container marg100">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-12">
            <div class="promo-block">
              <div class="promo-text">Contact Form</div>
              <div class="center-line"></div>
            </div>
            <div class="marg50">
              <div class="row">
                <div class="form-style" id="contact_form">
                  <div id="contact_body">
                    <div class="col-lg-4">
                      <p class="text_cont">
                          <asp:TextBox ID="txtName" runat="server" placeholder="Name" required CssClass="input-cont-textarea"></asp:TextBox></p>
                    </div>
                    <div class="col-lg-4">
                      <p class="text_cont">
                          <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" required CssClass="input-cont-textarea"></asp:TextBox></p>
                    </div>
                    
                    <div class="col-lg-4">
                      <p class="text_cont">
                          <asp:TextBox ID="txtContact" runat="server" placeholder="Contact No" required CssClass="input-cont-textarea"></asp:TextBox></p>
                    </div>
                    <div class="col-lg-4">
                      <p class="text_cont">
                          <asp:TextBox ID="txtSubject" runat="server" placeholder="Subject" required CssClass="input-cont-textarea"></asp:TextBox></p>
                    </div>
                    <div class="col-lg-12">
                      <p class="text_cont" style="margin-bottom:0px;">
                          <asp:TextBox ID="txtMessage" TextMode="MultiLine" placeholder="Message" runat="server" required class="input-cont-textarea" cols="40" rows="10"></asp:TextBox></p>
                    </div>
                    <div class="col-lg-12"><div id="contact_results"></div></div>
                    <div class="col-lg-12"><p>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                            CssClass="btn btn-default" onclick="btnSubmit_Click" /></p></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="promo-block">
              <div class="promo-text">Information</div>
              <div class="center-line"></div>
            </div>
            <div class="marg50">
                <ul class="contact-footer">
                  <li><strong><i class="icon-location"></i> Adress:</strong> Rejinagar (Near Bus Stop), Murshidabad, 742189</li>
                  <li><strong><i class="icon-mobile"></i> Phone:</strong> 8436703614 / 7001260308 / 9732515026</li>
                  <li><strong><i class="icon-mail"></i> E-mail:</strong> lifecareerservices@gmail.com</li>
                  <li><strong><i class="icon-key"></i> Time:</strong> from 10:00 am to 6:00 pm</li>
                </ul> 
                <hr> 
            </div>
          </div>
        </div>
      </div>
      <div>
          
          <table class="style1">
              <tr>
                  <td class="style2">
                      &nbsp;</td>
                  <td>
                  => Query / Feedback Veiw
                      <asp:GridView ID="gvQuery" runat="server" AutoGenerateColumns="False" 
                          BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
                          CellPadding="3">
                          <Columns>
                              <asp:BoundField DataField="qId" HeaderText="I D" Visible="false" />
                              <asp:BoundField DataField="name" HeaderText="Name" />
                              <asp:BoundField DataField="e_mail" HeaderText="E_Mail" />
                              <asp:BoundField DataField="contact" HeaderText="Contact No" />
                              <asp:BoundField DataField="sub" HeaderText="Subject" />
                              <asp:BoundField DataField="msg" HeaderText="Message" />
                          </Columns>
                          <FooterStyle BackColor="White" ForeColor="#000066" />
                          <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                          <PagerStyle ForeColor="#000066" HorizontalAlign="Left" BackColor="White" />
                          <RowStyle ForeColor="#000066" Height="30px" />
                          <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F1F1F1" />
                          <SortedAscendingHeaderStyle BackColor="#007DBB" />
                          <SortedDescendingCellStyle BackColor="#CAC9C9" />
                          <SortedDescendingHeaderStyle BackColor="#00547E" />
                      </asp:GridView>
                  </td>
              </tr>
          </table>
          
      </div>
      <script type="text/javascript" src="assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="assets/js/sticky.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.tweet.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/js/retina.js"></script>
    <script type="text/javascript" src="assets/js/jquery.cookie.html"></script>
    <script type="text/javascript" src="assets/js/switch.class.html"></script>
    <script type="text/javascript" src="assets/js/jquery.dlmenu.js"></script>
    <script type="text/javascript" src="assets/js/contacts.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
</asp:Content>
