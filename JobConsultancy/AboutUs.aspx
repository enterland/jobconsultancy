﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="JobConsultancy.AboutUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">.menu li.current a { color: #00C0E1 }</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name">About<span> Us</span></div></div>
<!--<div class="col-lg-6 pull-right"><div class="page-in-bread">
<span>You are here:</span> <a href="#">Home</a> \ About Us</div></div>-->
          </div>
        </div>
      </div>
      <div class="container marg75">
        <div class="row">
          <div class="col-lg-12">
            <div class="promo-block">
              <div class="promo-text">Mini Introduction</div>
              <div class="center-line"></div>
            </div>
            <br><br><br>
            <p align="justify">This company has been formed for the job seeker by the Life Career Services group. Open the door for the students and job seekers basis on todays need.
            Mission and vision is now to approach each and every job seeker and helping them building their career in managemet world.<br><br>
            <i>Learning is a lifelong process that extends beyond the classroom. So, why restrict yourself to traditional settings?</i>
 <br><br>
At Mind over Career Courses, we focus on courses that are far-reaching. We bring it wherever you are. By combining the fantastic power of crash courses, 
we provide our candidates real opportunity.
 <br><br>
The informal education workshops are unique learning opportunities that allow students to explore and discover the world around them using creativity and potential knowledge.
 <br><br>
We believe in the power of structured thinking and action. We don’t bring abstract visions, but a roadmap for growth with actionable steps.
<br><br>
 

<strong>Are you ready to start the future with bright career?</strong></p>
          </div>
        </div>
      </div>
      <div class="container marg75">
        <div class="row">
          <div class="col-lg-12">
            <div class="promo-block">
              <div class="promo-text">Our Team</div>
              <div class="center-line"></div>
            </div>
          </div>
        </div>
      </div> 
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 marg50">
            <div class="about-us"> 
              <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 col-ms-12"><img src="dir/2.jpg" alt=""></div>
                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 col-ms-12">
                  <div class="about-name">Name - ___________</div>
                  <div class="about-desc">Director</div>
                  <div class="about-texts">Detail</div>
                  <ul class="soc-about">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 marg50">
            <div class="about-us"> 
              <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 col-ms-12"><img src="dir/1.jpg" alt=""></div>
                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 col-ms-12">
                  <div class="about-name">Name - ___________</div>
                  <div class="about-desc">Director</div>
                  <div class="about-texts">Detail</div>
                  <ul class="soc-about">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                     
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 marg50">
            <div class="about-us"> 
              <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 col-ms-12"><img src="dir/3.jpg" alt=""></div>
                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 col-ms-12">
                  <div class="about-name">Name - ___________</div>
                  <div class="about-desc">CMD</div>
                  <div class="about-texts">Detail</div>
                  <ul class="soc-about">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                     
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!--<div class="col-lg-6 col-md-6 marg50">
            <div class="about-us"> 
              <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 col-ms-12"><img src="assets/images/serious.jpg" alt=""></div>
                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 col-ms-12">
                  <div class="about-name">Mark Pau</div>
                  <div class="about-desc">Smart programmer</div>
                  <div class="about-texts">Pellentesque luctus ac lorem id luctus aenean sagittis magna sit amet purus vehicsula tristique nunc a felis ultrices phasellus vitae ultrices lectus eget posuere. Aenean sagittis magna sit amet purus vehicsula.</div>
                  <ul class="soc-about">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-vk"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>-->
        </div>
      </div>
</asp:Content>
