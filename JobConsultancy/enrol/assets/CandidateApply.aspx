﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CandidateApply.aspx.cs" Inherits="JobConsultancy.enrol.assets.CandidateApply" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <link href="assets/images/favicon.html" rel="shortcut icon"/>
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144x144-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114x114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72x72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-precomposed.png" />
        <!-- JS FILES -->
        <script type="text/javascript" src="assets/js/jquery-1.20.2.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
        <!-- CSS FILES -->
        <link href="assets/css/style.css" media="screen" rel="stylesheet" type="text/css">
        <link href="assets/css/responsive.css" media="screen" rel="stylesheet" type="text/css">
        <link href="assets/css/flexslider.css" media="screen" rel="stylesheet" type="text/css">
        <link href="#" media="screen" rel="stylesheet" type="text/css" id="style-schem-color"> 
        <link href="#" media="screen" rel="stylesheet" type="text/css" id="style-schem"> 
                    <link rel="stylesheet" href="../../code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="container marg100">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">

                            <div class="marg50">
                                <div class="row">
                                    <div class="form-style" id="contact_form">
                                        <div id="contact_body">
                                            <div class="col-lg-12">
                                                <p class="text_cont">
                                                    <input type="text" name="applicant_name" id="name" placeholder="Name of the applicant (As in the high school certificate)" required class="input-cont-textarea">
                                                </p>
                                            </div>
                                            <div class="col-lg-12">
                                                <p class="text_cont">
                                                    <input type="text" name="applicant_father_name" placeholder="Father's Name" required class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-12"><p class="text_cont">
                                                    <input type="text" name="applicant_address" placeholder="Complete Address for correspondence (Do not repeat the name)" required class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-6"><p class="text_cont">
                                                    <input type="text" name="district" placeholder="District" required class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-6"><p class="text_cont">
                                                    <input type="text" name="state" placeholder="State" required class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-5"><p class="text_cont">
                                                    <input type="text" name="pincode" placeholder="Pincode" required class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-2"><p class="text_cont">
                                                    <input type="text" name="std_code" placeholder="STD Code"  class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-5"><p class="text_cont">
                                                    <input type="text" name="landline_number" placeholder="LandLine Number"  class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-6"><p class="text_cont">
                                                    <input type="text" name="mobile_no" placeholder="Mobile Number" required class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-6"><p class="text_cont">
                                                    <input type="text" name="email_id" placeholder="Email Id" required class="input-cont-textarea"></p>
                                            </div> 
                                            <div class="col-lg-3"><p class="text_cont">
                                                    <select name="gender" class="input-cont-textarea" required="">
                                                        <option value="">Select Gender</option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                        <option value="Others">Others</option>
                                                    </select>
                                                    <!--<input type="text" name="gender" placeholder="Gender" required class="input-cont-textarea"></p>-->
                                            </div>
                                            <div class="col-lg-3"><p class="text_cont">
                                                <select name="category" class="input-cont-textarea" required="">
                                                        <option value="">Select Category</option>
                                                        <option value="General">General</option>
                                                        <option value="SC">SC</option>
                                                        <option value="ST">ST</option>
                                                        <option value="OBC">OBC</option>
                                                    </select>
                                            </div>
                                            <div class="col-lg-3"><p class="text_cont">
                                                    <input type="text" name="nationality" placeholder="Nationality" required class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-3"><p class="text_cont">
                                                    <input type="text" name="dob" id="dob" placeholder="Date of Birth" required class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-12">
                                                <table width="100%" class="table table-bordered">
                                                    <thead>
                                                        <tr ><th>Examination</th><th>Year</th><th>Board/Universitry</th><th>Div/Grade</th><th>Percentage</th></tr></thead>
                                                    <tbody>
                                                        <tr><th><input type="text" name="exam_1"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="exam_year_1"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="board_1"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="grade_1"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="percentage_1"  class="input-cont-textarea"></th></tr>

                                                        <tr><th><input type="text" name="exam_2"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="exam_year_2"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="board_2"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="grade_2"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="percentage_2"  class="input-cont-textarea"></th></tr>

                                                        <tr><th><input type="text" name="exam_3"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="exam_year_3"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="board_3"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="grade_3"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="percentage_3"  class="input-cont-textarea"></th></tr>

                                                        <tr><th><input type="text" name="exam_4"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="exam_year_4"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="board_4"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="grade_4"  class="input-cont-textarea"></th>
                                                            <th><input type="text" name="percentage_4"  class="input-cont-textarea"></th></tr>
                                                    </tbody>
                                                </table></div>

                                            <div class="col-lg-12"><p class="text_cont">
                                                    <select required class="input-cont-textarea" name="applying_for">
                                                        <option value="">Applying for:</option>
                                                        <option value="aviation - CAGHP | 3 – 6 classes">Aviation  - CAGHP ( 3 – 6 classes )</option>
                                                        <option value="aviation - CCHP | 3 classes">Aviation  - CCHP ( 3 classes )</option>
                                                        <option value="aviation - CUHP | 3 classes">Aviation  - CUHP ( 3 classes )</option>
                                                        <option value="banking - CBF | 9 Classes">Banking - CBF ( 9 Classes )</option>
                                                        <option value="banking - CBS | 9 Classes">Banking - CBS ( 9 Classes )</option>
                                                    </select></p>
                                            </div>
                                            
                                             <div class="col-lg-12">
                                                <p>Please Upload your Address Proof & Educational Certificate. </p></div>
                                            <div class="col-lg-3">
                                                     
                                                    <p class="text_cont">
                                                    <input type="file" name="file_1"   class="input-cont-textarea"></p>
                                            </div>
                                            <div class="col-lg-3"><p class="text_cont">
                                                    <input type="file" name="file_2"   class="input-cont-textarea"></p>
                                            </div>

                                            <div class="col-lg-12"><p><input type="submit" id="submit_btn" class="btn btn-default" value="Proceed to Payment" /></p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="promo-block">
                                <div class="promo-text">Information</div>
                                <div class="center-line"></div>
                            </div>
                            <div class="marg50">
                                <ul class="contact-footer">
                  <li><strong><i class="icon-location"></i> Dunlop Adress:</strong> 138/1, BT Road,Dunlop, Kolkata</li>
                  <li><strong><i class="icon-mobile"></i> Phone:</strong> 03365230014 / 03365030015</li>
				  <li><strong><i class="icon-location"></i>Madhyamgram Adress:</strong> Puspa Villa Apartments, 258/A, Madhyamgram Basunagar, 1 No. Gate, Kolkata - 700129</li>
                  <li><strong><i class="icon-mobile"></i> Phone:</strong> 03369000254 / 03369000255</li> 
                  <li><strong><i class="icon-videocam"></i> Skype:</strong> admin@careergraphinstitute.com</li>
                  <li><strong><i class="icon-mail"></i> E-mail:</strong> support@careergraphinstitute.com</li>
                  <li><strong><i class="icon-key"></i> Time:</strong> from 10:30 am to 6 pm</li>
                </ul> 
                                <!-- <ul class="soc-contacts">
                                   <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                   <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    
                                   <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                   <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                    
                                   <li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
                                 </ul> -->
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript" src="assets/js/waypoints.min.js"></script>
            <script type="text/javascript" src="assets/js/sticky.js"></script>
            <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/js/jquery.tweet.min.js"></script>
            <script type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
            <script type="text/javascript" src="assets/js/retina.js"></script>
            <script type="text/javascript" src="assets/js/jquery.cookie.html"></script>
            <script type="text/javascript" src="assets/js/switch.class.html"></script>
            <script type="text/javascript" src="assets/js/jquery.dlmenu.js"></script>
            <script type="text/javascript" src="assets/js/contacts.js"></script>
            <script type="text/javascript" src="assets/js/main.js"></script>

  <script src="../../code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#dob" ).datepicker({
        dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-50:+50',
    });
  } );
  </script>
        </form>
    
</asp:Content>
