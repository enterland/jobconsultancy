﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CandidateEdit.aspx.cs" Inherits="JobConsultancy.CandidateEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        .style1
        {
            width: 100%;
            background-color: antiquewhite;
        }
        .style3
        {
            width: 270px;
            
        }
        .style4
        {
            width: 22px;
             padding:10px;
        }
        .style5
    {
        height: 58px;
        
    }
    .style6
    {
        width: 280px;
         
    }
   
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>

            <td>
                <table class="style1">
                    <tr>
                        <td align="center" class="style5" 
                            
                            style="font-family: Andalus; font-size: xx-large; text-decoration: underline;" 
                            colspan="2">
                            Edit Student</td>
                    </tr>
                    <tr>
                        <td class="style6">
                            &nbsp;</td>
                        <td>
                            <table class="style1">
                                <tr>
                                    <td align="right" class="style3">
                                        Student Contact No.</td>
                                    <td align="center" class="style4">
                                        :</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStdPh" runat="server" Width="240px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="style3">
                                        Student Name</td>
                                    <td align="center" class="style4">
                                        :</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStdNam" runat="server" Width="240px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="style3">
                                        Sex</td>
                                    <td align="center" class="style4">
                                        :</td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True">Male</asp:ListItem>
                                            <asp:ListItem>Female</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="style3">
                                        Date Of Birth</td>
                                    <td align="center" class="style4">
                                        :</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDob" runat="server" Width="240px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="style3">
                                        E_Mail</td>
                                    <td align="center" class="style4">
                                        :</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server" Width="240px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            Academic Qualification :<br />
                            <br />
                            <asp:gridview ID="Gridview1"  runat="server"  ShowFooter="true"  
                             AutoGenerateColumns="false"  
                             OnRowCreated="Gridview1_RowCreated" Width="200px">  
    <Columns>  
        <asp:BoundField DataField="RowNumber" HeaderText="Sl" />  
        <asp:TemplateField HeaderText="Course Name">  
            <ItemTemplate>  
                <asp:DropDownList ID="DropDownList1" runat="server"  
                                          AppendDataBoundItems="true">  
                    <asp:ListItem Value="-1">Select</asp:ListItem>  
                </asp:DropDownList> 
            </ItemTemplate>  
        </asp:TemplateField>  
        <asp:TemplateField HeaderText="Board / University">  
            <ItemTemplate>  
                <asp:TextBox ID="txtBoard" runat="server"></asp:TextBox>  
            </ItemTemplate>  
        </asp:TemplateField>  
        <asp:TemplateField  HeaderText="Passing Year">  
            <ItemTemplate>  
                <asp:TextBox ID="txtPyr" runat="server"></asp:TextBox>  
            </ItemTemplate>  
        </asp:TemplateField>  
        <asp:TemplateField HeaderText="(%)Marks / Grade">  
            <ItemTemplate>  
               <asp:TextBox ID="txtMark" runat="server"></asp:TextBox>   
            </ItemTemplate>  
            <FooterStyle HorizontalAlign="Right" />  
            <FooterTemplate>  
                <asp:Button ID="ButtonAdd" runat="server"   
                                     Text="Add New Row"   
                                     onclick="ButtonAdd_Click" />  
            </FooterTemplate>  
        </asp:TemplateField>  
        <asp:TemplateField>  
            <ItemTemplate>  
                <asp:LinkButton ID="LinkButton1" runat="server"   
                                        onclick="LinkButton1_Click">Remove</asp:LinkButton>  
            </ItemTemplate>  
        </asp:TemplateField>  
    </Columns>  
</asp:gridview>  
                            <br />
                            Extar Qualification / Expriences / Training :<br />
                           <br />
                            <asp:gridview ID="Gridview2"  runat="server"  ShowFooter="true"  
                             AutoGenerateColumns="false"  
                             OnRowCreated="Gridview2_RowCreated" Width="350px">  
    <Columns>  
        <asp:BoundField DataField="RowNumber" HeaderText="Sl" />  
        <asp:TemplateField HeaderText="Name">  
            <ItemTemplate>  
                <asp:TextBox ID="DropDownList2" runat="server">  
                </asp:TextBox> 
            </ItemTemplate>  
        </asp:TemplateField>  
        <asp:TemplateField HeaderText="Organization">  
            <ItemTemplate>  
                <asp:TextBox ID="txtOrg" runat="server"></asp:TextBox>  
            </ItemTemplate>  
        </asp:TemplateField>  
        <asp:TemplateField  HeaderText="Year">  
            <ItemTemplate>  
                <asp:TextBox ID="txtYr" runat="server"></asp:TextBox>  
            </ItemTemplate>  
        </asp:TemplateField>  
        <asp:TemplateField HeaderText="Remarks">  
            <ItemTemplate>  
               <asp:TextBox ID="txtRemark" runat="server"></asp:TextBox>   
            </ItemTemplate>  
            <FooterStyle HorizontalAlign="Right" />  
            <FooterTemplate>  
                <asp:Button ID="ButtonAdd2" runat="server"   
                                     Text="Add New Row"   
                                     onclick="ButtonAdd2_Click" />  
            </FooterTemplate>  
        </asp:TemplateField>  
        <asp:TemplateField>  
            <ItemTemplate>  
                <asp:LinkButton ID="LinkButton2" runat="server"   
                                        onclick="LinkButton2_Click">Remove</asp:LinkButton>  
            </ItemTemplate>  
        </asp:TemplateField>  
    </Columns>  
</asp:gridview>
                            <br />
                            Upload Resume :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                            &nbsp;&nbsp;
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="style6">
                            &nbsp;</td>
                        <td align="center">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br />
                            <asp:Button ID="btnSbmt" runat="server" Height="34px" onclick="btnSbmt_Click1" 
                                Text="SUBMIT" Width="140px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style6">
                            &nbsp;</td>
                        <td>
                            <asp:Label ID="labDisp" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
