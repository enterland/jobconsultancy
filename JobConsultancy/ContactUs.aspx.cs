﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobConsultancy
{
    public partial class ContactUs : System.Web.UI.Page
    {
        myJobDataClassesDataContext db;
        protected void Page_Load(object sender, EventArgs e)
        {
            db=new myJobDataClassesDataContext();
            if (!IsPostBack)
            {
                
                if (Session["uid"] != null)
                {
                    var auth = from m in db.userControls where  m.uid == Session["uid"] && m.authType == 1 select m;
                    if (auth.Count() >= 1)
                        getQData();
                }
            }
        }

        private void getAuthQueryList()
        {
           
        }

        private void getQData()
        {
            db = new myJobDataClassesDataContext();

            var qg = db.contactsQueries.Select(m => new 
            { qId = m.qId, name = m.name, e_mail = m.e_mail, contact = m.contact, sub = m.sub, msg = m.msg }).ToList();

            gvQuery.DataSource = qg;
            gvQuery.DataBind();

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            db = new myJobDataClassesDataContext();

            contactsQuery q = new contactsQuery
            {
                name = txtName.Text,
                e_mail = txtEmail.Text,
                contact = txtContact.Text,
                sub = txtSubject.Text,
                msg = txtMessage.Text
            };

            db.contactsQueries.InsertOnSubmit(q);
            db.SubmitChanges();

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), 
                "Sucessfull", "alert('Dear "+txtName.Text+", Your Query/Feedback Successfully Submited, The Management Team Contact to you Soon')", true);

            try
            {
                var auth = db.userControls.Where(m => m.uid == Session["uid"]).FirstOrDefault();
                if (auth.authType == 1)
                    getQData();
            }
            catch (Exception)
            {


            }
        }
    }
}