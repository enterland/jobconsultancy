﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Text;
using System.Configuration;
namespace JobConsultancy
{
    public partial class CandidateEdit : System.Web.UI.Page
    {
        myJobDataClassesDataContext mdc;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetInitialRow();
                gdata();
                SetInitialRow2();
               
            }
        }

        public void gdata()
        {
            mdc = new myJobDataClassesDataContext();
            string ph = Request.QueryString["ph"];

            txtStdPh.Text = ph;

            student st = mdc.students.Single(s => s.stdPh == ph);
            txtDob.Text = st.dob;
            txtEmail.Text = st.eMail;
            txtStdNam.Text = st.stdName;
            RadioButtonList1.SelectedValue = st.sex;

        }
        private void FillDropDownList(DropDownList ddl)
        {
            mdc = new myJobDataClassesDataContext();

            var dd = from m in mdc.courses
                     select new
                     {
                         m.courseId,
                         m.courseName
                     };
            ddl.DataSource = dd;
            ddl.DataValueField = "courseId";
            ddl.DataValueField = "courseName";
            ddl.DataBind();
        }


        private void SetInitialRow()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Course Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Board / University", typeof(string)));
            dt.Columns.Add(new DataColumn("Passing Year", typeof(string)));
            dt.Columns.Add(new DataColumn("(%)Marks / Grade", typeof(string)));

            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Board / University"] = string.Empty;
            dr["Passing Year"] = string.Empty;
            dr["(%)Marks / Grade"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState for future reference   
            ViewState["CurrentTable"] = dt;

            //Bind the Gridview   
            Gridview1.DataSource = dt;
            Gridview1.DataBind();

            //After binding the gridview, we can then extract and fill the DropDownList with Data   
            DropDownList ddl1 = (DropDownList)Gridview1.Rows[0].Cells[1].FindControl("DropDownList1");

            FillDropDownList(ddl1);

        }


        private void AddNewRowToGrid()
        {

            if (ViewState["CurrentTable"] != null)
            {

                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                    //add new row to DataTable   
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    //Store the current data to ViewState for future reference   

                    ViewState["CurrentTable"] = dtCurrentTable;


                    for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                    {

                        //extract the TextBox values   

                        TextBox box1 = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("txtBoard");
                        TextBox box2 = (TextBox)Gridview1.Rows[i].Cells[3].FindControl("txtPyr");
                        TextBox box3 = (TextBox)Gridview1.Rows[i].Cells[4].FindControl("txtMark");

                        dtCurrentTable.Rows[i]["Board / University"] = box1.Text;
                        dtCurrentTable.Rows[i]["Passing Year"] = box2.Text;
                        dtCurrentTable.Rows[i]["(%)Marks / Grade"] = box3.Text;



                        //extract the DropDownList Selected Items   

                        DropDownList ddl1 = (DropDownList)Gridview1.Rows[i].Cells[1].FindControl("DropDownList1");

                        // Update the DataRow with the DDL Selected Items   

                        dtCurrentTable.Rows[i]["Course Name"] = ddl1.SelectedItem.Text;


                    }

                    //Rebind the Grid with the current data to reflect changes   
                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");

            }
            //Set Previous Data on Postbacks   
            SetPreviousData();
        }

        private void SetPreviousData()
        {

            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox box1 = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("txtBoard");
                        TextBox box2 = (TextBox)Gridview1.Rows[i].Cells[3].FindControl("txtPyr");
                        TextBox box3 = (TextBox)Gridview1.Rows[i].Cells[4].FindControl("txtMark");

                        DropDownList ddl1 = (DropDownList)Gridview1.Rows[rowIndex].Cells[1].FindControl("DropDownList1");


                        //Fill the DropDownList with Data   
                        FillDropDownList(ddl1);


                        if (i < dt.Rows.Count - 1)
                        {

                            //Assign the value from DataTable to the TextBox   
                            box1.Text = dt.Rows[i]["Board / University"].ToString();
                            box2.Text = dt.Rows[i]["Passing Year"].ToString();
                            box3.Text = dt.Rows[i]["(%)Marks / Grade"].ToString();

                            //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                            ddl1.ClearSelection();
                            ddl1.Items.FindByText(dt.Rows[i]["Course Name"].ToString()).Selected = true;



                        }

                        rowIndex++;
                    }
                }
            }
        }


        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        protected void Gridview1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("LinkButton1");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (gvRow.RowIndex < dt.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        dt.Rows.Remove(dt.Rows[rowID]);
                        ResetRowID(dt);
                    }
                }

                //Store the current data in ViewState for future reference  
                ViewState["CurrentTable"] = dt;

                //Re bind the GridView for the updated data  
                Gridview1.DataSource = dt;
                Gridview1.DataBind();
            }

            //Set Previous Data on Postbacks  
            SetPreviousData();
        }

        private void ResetRowID(DataTable dt)
        {
            int rowNumber = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[0] = rowNumber;
                    rowNumber++;
                }
            }
        }



     
        private void SetInitialRow2()
        {

            DataTable dt2 = new DataTable();
            DataRow dr2 = null;

            dt2.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt2.Columns.Add(new DataColumn("Name", typeof(string)));//for TextBox value   
            dt2.Columns.Add(new DataColumn("Organization", typeof(string)));//for TextBox value   
            dt2.Columns.Add(new DataColumn("Year", typeof(string)));//for DropDownList selected item   
            dt2.Columns.Add(new DataColumn("Remark", typeof(string)));//for DropDownList selected item   

            dr2 = dt2.NewRow();
            dr2["RowNumber"] = 1;
            dr2["Organization"] = string.Empty;
            dr2["Year"] = string.Empty;
            dr2["Remark"] = string.Empty;

            dt2.Rows.Add(dr2);

            //Store the DataTable in ViewState for future reference   
            ViewState["CurrentTable2"] = dt2;

            //Bind the Gridview   
            Gridview2.DataSource = dt2;
            Gridview2.DataBind();

            //After binding the gridview, we can then extract and fill the DropDownList with Data   
            //DropDownList ddl1 = (DropDownList)Gridview2.Rows[0].Cells[1].FindControl("DropDownList2");

           // FillDropDownList2(ddl1);

        }

        private void AddNewRowToGrid2()
        {

            if (ViewState["CurrentTable2"] != null)
            {

                DataTable dt2CurrentTable = (DataTable)ViewState["CurrentTable2"];
                DataRow dr2CurrentRow = null;

                if (dt2CurrentTable.Rows.Count > 0)
                {
                    dr2CurrentRow = dt2CurrentTable.NewRow();
                    dr2CurrentRow["RowNumber"] = dt2CurrentTable.Rows.Count + 1;

                    //add new row to DataTable   
                    dt2CurrentTable.Rows.Add(dr2CurrentRow);
                    //Store the current data to ViewState for future reference   

                    ViewState["CurrentTable2"] = dt2CurrentTable;


                    for (int i = 0; i < dt2CurrentTable.Rows.Count - 1; i++)
                    {

                        //extract the TextBox values   

                        TextBox box4 = (TextBox)Gridview2.Rows[i].Cells[2].FindControl("txtOrg");
                        TextBox box5 = (TextBox)Gridview2.Rows[i].Cells[3].FindControl("txtYr");
                        TextBox box6 = (TextBox)Gridview2.Rows[i].Cells[4].FindControl("txtRemark");

                        dt2CurrentTable.Rows[i]["Organization"] = box4.Text;
                        dt2CurrentTable.Rows[i]["Year"] = box5.Text;
                        dt2CurrentTable.Rows[i]["Remark"] = box6.Text;

                        //extract the DropDownList Selected Items   

                        DropDownList ddl1 = (DropDownList)Gridview2.Rows[i].Cells[1].FindControl("DropDownList2");

                        // Update the DataRow with the DDL Selected Items   

                        dt2CurrentTable.Rows[i]["Name"] = ddl1.SelectedItem.Text;


                    }

                    //Rebind the Grid with the current data to reflect changes   
                    Gridview2.DataSource = dt2CurrentTable;
                    Gridview2.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");

            }
            //Set Previous Data on Postbacks   
            SetPreviousData2();
        }

        private void SetPreviousData2()
        {

            int rowIndex2 = 0;
            if (ViewState["CurrentTable2"] != null)
            {

                DataTable dt2 = (DataTable)ViewState["CurrentTable2"];
                if (dt2.Rows.Count > 0)
                {

                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        TextBox box4 = (TextBox)Gridview2.Rows[i].Cells[2].FindControl("txtOrg");
                        TextBox box5 = (TextBox)Gridview2.Rows[i].Cells[3].FindControl("txtYr");
                        TextBox box6 = (TextBox)Gridview2.Rows[i].Cells[4].FindControl("txtRemark");

                        DropDownList ddl1 = (DropDownList)Gridview2.Rows[rowIndex2].Cells[1].FindControl("DropDownList2");


                        //Fill the DropDownList with Data   
                        //FillDropDownList2(ddl1);


                        if (i < dt2.Rows.Count - 1)
                        {

                            //Assign the value from DataTable to the TextBox   
                            box4.Text = dt2.Rows[i]["Organization"].ToString();
                            box5.Text = dt2.Rows[i]["Year"].ToString();
                            box6.Text = dt2.Rows[i]["Remark"].ToString();

                            //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                            ddl1.ClearSelection();
                            ddl1.Items.FindByText(dt2.Rows[i]["Name"].ToString()).Selected = true;



                        }

                        rowIndex2++;
                    }
                }
            }
        }


        protected void ButtonAdd2_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid2();
        }

        protected void Gridview2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt2 = (DataTable)ViewState["CurrentTable2"];
                LinkButton lb = (LinkButton)e.Row.FindControl("LinkButton2");
                if (lb != null)
                {
                    if (dt2.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt2.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow2 = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow2.RowIndex;
            if (ViewState["CurrentTable2"] != null)
            {

                DataTable dt2 = (DataTable)ViewState["CurrentTable2"];
                if (dt2.Rows.Count > 1)
                {
                    if (gvRow2.RowIndex < dt2.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        dt2.Rows.Remove(dt2.Rows[rowID]);
                        ResetRowID2(dt2);
                    }
                }

                //Store the current data in ViewState for future reference  
                ViewState["CurrentTable2"] = dt2;

                //Re bind the GridView for the updated data  
                Gridview2.DataSource = dt2;
                Gridview2.DataBind();
            }

            //Set Previous Data on Postbacks  
            SetPreviousData2();
        }

        private void ResetRowID2(DataTable dt2)
        {
            int rowNumber2 = 1;
            if (dt2.Rows.Count > 0)
            {
                foreach (DataRow row in dt2.Rows)
                {
                    row[0] = rowNumber2;
                    rowNumber2++;
                }
            }
        }

        

        protected void btnSbmt_Click1(object sender, EventArgs e)
        {
            mdc = new myJobDataClassesDataContext();

            student std = new student();
            std.stdPh = txtStdPh.Text;
            std.stdName = txtStdNam.Text;
            std.sex = RadioButtonList1.SelectedValue;
            std.dob = txtDob.Text;
            std.eMail = txtEmail.Text;

            mdc.students.InsertOnSubmit(std);
            mdc.SubmitChanges();

            //Save Data to Qualification table
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        qualification q = new qualification();
                        //extract the TextBox values  
                        TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtBoard");
                        TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtPyr");
                        TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("txtMark");
                        DropDownList ddl1 = (DropDownList)Gridview1.Rows[rowIndex].Cells[1].FindControl("DropDownList1");
                        q.stdPh = txtStdPh.Text;
                        q.board = box1.Text;
                        q.pYear = box2.Text;
                        q.marks = box3.Text;
                        course c = mdc.courses.Single(s => s.courseName == ddl1.SelectedItem.Value);
                        q.courseId = c.courseId;


                        mdc.qualifications.InsertOnSubmit(q);
                        mdc.SubmitChanges();

                        rowIndex++;
                    }

                }
            }

            //Save Data to Extra Qualification table
            try
            {
                int rowIndex2 = 0;
                if (ViewState["CurrentTable2"] != null)
                {
                    DataTable dt2CurrentTable = (DataTable)ViewState["CurrentTable2"];
                    if (dt2CurrentTable.Rows.Count > 0)
                    {
                        for (int i = 1; i <= dt2CurrentTable.Rows.Count; i++)
                        {
                            ExtraQualification q = new ExtraQualification();
                            //extract the TextBox values 
                            TextBox ddl1 = (TextBox)Gridview2.Rows[rowIndex2].Cells[1].FindControl("DropDownList2");
                            TextBox box1 = (TextBox)Gridview2.Rows[rowIndex2].Cells[2].FindControl("txtOrg");
                            TextBox box2 = (TextBox)Gridview2.Rows[rowIndex2].Cells[3].FindControl("txtYr");
                            TextBox box3 = (TextBox)Gridview2.Rows[rowIndex2].Cells[4].FindControl("txtRemark");

                            q.stdPh = txtStdPh.Text;
                            q.organisation = box1.Text;
                            q.pYear = box2.Text;
                            q.remark = box3.Text;
                            q.pCourseId = ddl1.Text;


                            mdc.ExtraQualifications.InsertOnSubmit(q);
                            mdc.SubmitChanges();

                            rowIndex2++;
                        }

                    }
                }
            }
            catch (Exception)
            {
                
                
            }

            //Resume Upload
            string fname, spath, ext;
            spath = MapPath("upd");
            fname = FileUpload1.FileName;
            if (fname == "")
            {
                labDisp.Text = "Select a dile first.";
            }
            else
            {

                ext = Path.GetExtension(fname);
                if (ext == ".pdf" || ext == ".doc")
                {
                    spath += @"\" + txtStdPh.Text + ext;
                    FileUpload1.SaveAs(spath);
                    string cvurl = "~/upd/" + txtStdPh.Text;
                    //ListItem li = new ListItem(fname, imgurl);

                    labDisp.Text = "Resume is uploaded";
                }
                else
                {
                    labDisp.Text = "this type of file is not upload.";
                }
            }

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);

            txtDob.Text = "";
            txtEmail.Text = "";
            txtStdNam.Text = "";
            txtStdPh.Text = "";
        }
        
        

    }
}