﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="JobConsultancy.Gallery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<meta content="xenia - responsive and retina ready template" name="description">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
    <link href="assets/images/favicon.html" rel="shortcut icon"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144x144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-precomposed.png" />
    <!-- JS FILES -->
    <script type="text/javascript" src="assets/js/jquery-1.20.2.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
    <!-- CSS FILES -->
    <link href="assets/css/cubeportfolio-4.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" media="screen" rel="stylesheet" type="text/css">
    <link href="assets/css/responsive.css" media="screen" rel="stylesheet" type="text/css">
<div class="page-in">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 pull-left"><div class="page-in-name">Gallery <span></span></div></div>
           
          </div>
        </div>
      </div>
      <div class="container marg50">
        <div class="row">
          <div class="col-lg-12">
            <div id="filters-container-portfolio-4" class="cbp-l-filters-button">
              <button data-filter="*" class="cbp-filter-item-active cbp-filter-item">All<div class="cbp-filter-counter"></div></button>
              <button data-filter=".wordpress" class="cbp-filter-item">Images<div class="cbp-filter-counter"></div></button>
              <button data-filter=".design" class="cbp-filter-item">Videos<div class="cbp-filter-counter"></div></button>
             
            </div>
          </div>
        </div>
      </div>
      <div class="container marg50">
        <div class="grid hover-3">
          <div class="cbp-l-grid-projects" id="grid-container-portfolio-4">
            <ul>
              <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                   <a href="event/careergraph institute of professionals.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals.jpg" alt=""></a>
                    
                  </figure> 
                </div>
              </li>
              <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                   <a href="event/careergraph%20institute%20of%20professionals_1.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_1.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
              <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                   <a href="event/careergraph%20institute%20of%20professionals_2.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_2.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
              <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                   <a href="event/careergraph%20institute%20of%20professionals_3.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_3.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
              <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                 <figure>
                   <a href="event/careergraph%20institute%20of%20professionals_4.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_4.jpg" alt=""></a>
                    
                  </figure> 
                </div>
              </li>
              <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_5.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_5.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_6.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_6.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_7.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_7.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_8.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_8.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_9.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_9.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_10.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_10.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_11.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_11.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_12.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_12.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_13.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_13.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_14.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_14.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_15.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_15.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_16.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_16.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_17.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_17.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  
			  <%--<li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_19.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_19.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_20.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_20.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_21.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_21.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>
			  <li class="cbp-item wordpress html">
                <div class="portfolio-main">
                  <figure>
                    
                   <a href="event/careergraph%20institute%20of%20professionals_22.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph%20institute%20of%20professionals_22.jpg" alt=""></a>
                    
                  </figure>
                </div>
              </li>--%>
			  
            <!-- <li class="cbp-item logo">
                <div class="portfolio-main">
                 <figure>
                   <a href="event/careergraph institute of professionals_4.jpg" class="portfolio-attach cbp-lightbox" data-title="Gadgets<br>by Jacob Cummings"> <img src="event/careergraph institute of professionals_4.jpg" alt=""></a>
                    
                  </figure> 
                </div>
              </li>-->
             
            </ul>
          </div>
         
        </div>  
      </div>
      <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="assets/js/sticky.js"></script>
    <script type="text/javascript" src="assets/js/jquery.tweet.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/js/retina.js"></script>
    <script type="text/javascript" src="assets/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="assets/js/portfolio-4.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dlmenu.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
</asp:Content>
