﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;

namespace JobConsultancy
{
    public partial class CandidateDetail : System.Web.UI.Page
    {
        myJobDataClassesDataContext mdc;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gdata();
                gdata2();
            }
        }

        public void gdata()
        {
            mdc = new myJobDataClassesDataContext();
            string ph = Request.QueryString["ph"];

            txtStdPh.Text = ph;

            student st = mdc.students.Single(s => s.stdPh == ph);
            txtDob.Text = st.dob;
            txtEmail.Text = st.eMail;
            txtStdNam.Text = st.stdName;
            RadioButtonList1.SelectedValue = st.sex;

            var crs = from cr in mdc.qualifications
                      where
                      cr.stdPh == ph
                      select cr;



            DataTable DtSCrs = new DataTable();

            DtSCrs.Columns.Add(new DataColumn("Course Name", Type.GetType("System.String")));
            DtSCrs.Columns.Add(new DataColumn("Board / University", Type.GetType("System.String")));
            DtSCrs.Columns.Add(new DataColumn("Passing Year", Type.GetType("System.String")));
            DtSCrs.Columns.Add(new DataColumn("(%) Mark / Garde", Type.GetType("System.String")));

            DataRow dr;

            foreach (qualification q in crs)
            {
                dr = DtSCrs.NewRow();
                dr[0] = q.course.courseName;
                dr[1] = q.board;
                dr[2] = q.pYear;
                dr[3] = q.marks;

                DtSCrs.Rows.Add(dr);
            }

            GridView3.DataSource = DtSCrs;
            GridView3.DataBind();

        }

        public void gdata2()
        {
            mdc = new myJobDataClassesDataContext();
            string ph = Request.QueryString["ph"];


            var crs = from cr in mdc.ExtraQualifications
                      where
                      cr.stdPh == ph
                      select cr;



            DataTable DtSCrs2 = new DataTable();

            DtSCrs2.Columns.Add(new DataColumn("Name", Type.GetType("System.String")));
            DtSCrs2.Columns.Add(new DataColumn("Organization", Type.GetType("System.String")));
            DtSCrs2.Columns.Add(new DataColumn("Year", Type.GetType("System.String")));
            DtSCrs2.Columns.Add(new DataColumn("Remarks", Type.GetType("System.String")));

            DataRow drr;

            foreach (ExtraQualification q in crs)
            {
                drr = DtSCrs2.NewRow();
                drr[0] = q.pCourseId;
                drr[1] = q.organisation;
                drr[2] = q.pYear;
                drr[3] = q.remark;

                DtSCrs2.Rows.Add(drr);
            }

            GridView4.DataSource = DtSCrs2;
            GridView4.DataBind();

        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            string fname = txtStdPh.Text + ".pdf";
            string fpath = MapPath(@"upd\" + fname);

            Response.Clear();

            Response.ClearHeaders();
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition", "attachment; filename=" + fname);


            WebClient wc = new WebClient();
            byte[] data = wc.DownloadData(fpath);
            Response.ContentType = "application/doc";
            Response.BinaryWrite(data);
            Response.End();
        }
    }
}