﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobConsultancy
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        myJobDataClassesDataContext db;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
            pnlShow();
        }

        protected void lbtnSignIn_Click(object sender, EventArgs e)
        {
            db = new myJobDataClassesDataContext();

            var u = db.userControls.Where(m => m.uid == txtUid.Text && m.pass == txtPass.Text).FirstOrDefault();
            
            if (u != null)
            {
                Session["uid"] = txtUid.Text;
                 labUserName.Text ="Hi, "+ Session["uid"].ToString();
               
                pnlShow();
                Response.Redirect("Home.aspx");
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Sgin up Error!!", "alert('Invalid User Name / Password')", true);
        }

        public void pnlShow()
        {
            if (Session["uid"] != null)
            {
                pnlLog.Visible = false;
                pnlLogout.Visible = true;
                labUserName.Text = "Hi, " + Session["uid"].ToString();
            }
            else
            {
                 pnlLog.Visible = true;
                 pnlLogout.Visible = false;
                 
            }
        }
       

        protected void LinkButton1_Click1(object sender, EventArgs e)
        {
            Session.Abandon();
            Session.Clear();
            Session["uid"] = "";

            pnlLog.Visible = true;
            pnlLogout.Visible = false;

            Response.Redirect("Home.aspx");
            
        }
    }
}