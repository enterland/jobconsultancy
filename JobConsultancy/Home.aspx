﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="JobConsultancy.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<link href="#" rel="shortcut icon"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144x144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-precomposed.png" />
    <!-- JS FILES -->
    <script type="text/javascript" src="assets/js/jquery-1.20.2.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
    <!-- CSS FILES -->
    <link href="assets/css/style.css" media="screen" rel="stylesheet" type="text/css">
    <link href="assets/css/responsive.css" media="screen" rel="stylesheet" type="text/css">
    <link href="assets/rs-plugin/css/settings.css" media="screen" rel="stylesheet" type="text/css">
    <link href="assets/css/navstylechange.css" media="screen" rel="stylesheet" type="text/css">
    <link href="assets/css/cubeportfolio.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="assets/css/testimonialrotator.css" media="screen" rel="stylesheet" type="text/css"> 
     <div class="tp-banner-container" style="height:500px;">
        <div class="tp-banner" >
          <ul style="display:none;">
            <li data-transition="fade" data-slotamount="7" data-masterspeed="700" >
 <img src="assets/images/fon1.jpg"  alt=""  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
             </li>
            <li data-transition="random" data-slotamount="7" data-masterspeed="700" >
 <img src="assets/images/fon2.jpg"  alt=""  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
              </li>
            <li data-transition="random" data-slotamount="7" data-masterspeed="700" >
 <img src="assets/images/fon3.jpg"  alt=""  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
               
            </div>
          </li>
        </ul>
      </div>
<div class="prl-1">
        <div class="prlx">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 marg50"><div class="quote"><h1 style="color:#FFF">Latest News</h1></div></div>
              <div class="col-lg-12">
                <div class="testimonials">
                  <div id="carousel-testimonials" class="carousel slide">
                    <ol class="carousel-indicators">
                      <li data-target="#carousel-testimonials" data-slide-to="0" class="active"></li>
                      <li data-target="#carousel-testimonials" data-slide-to="1" class=""></li>
                      <!--<li data-target="#carousel-testimonials" data-slide-to="2" class=""></li>
                      <li data-target="#carousel-testimonials" data-slide-to="3" class=""></li>-->
                    </ol>
                    

                    <%--<a href = "javascript:showLightBox()" onMouseOut="javascript:stopShowLightBox()" style="cursor: pointer;">Click to start Lightbox gallery</a>--%>
                  
                    <div class="carousel-inner">
                      <div class="item active">
                        <p class="testimonial-quote"><img src="new.gif"><a class="vlightbox" href="/assets/LatestNewsImg/1.jpg" title="2" id="firstImage">
                    <img src="" onMouseOver="javascript:showLightBox()" onMouseOut="javascript:stopShowLightBox()" style="cursor: pointer;">New Admission</a>
                    <a class="vlightbox" href="/assets/LatestNewsImg/1.jpg" title="2" id="A1"></p>
                        <!--<p class="testimonial-author">Apply Now</p>-->
                      </div>
                      <div class="item">
                         <p class="testimonial-quote"><img src="new.gif"><a class="vlightbox" href="/assets/LatestNewsImg/2.jpg" title="2" id="A2">
                    <img src="" onMouseOver="javascript:showLightBox()" onMouseOut="javascript:stopShowLightBox()" style="cursor: pointer;">Click to View Banner</a>
                    <a class="vlightbox" href="/assets/LatestNewsImg/2.jpg" title="2" id="A3"></p>
                        <!--<p class="testimonial-author">Apply Now</p>-->
                      </div>
                      
                    </div>
                    <a class="left carousel-control" href="#carousel-testimonials" data-slide="prev">&lsaquo;</a>
                    <a class="right carousel-control" href="#carousel-testimonials" data-slide="next">&rsaquo;</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      

      <div class="container marg75">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="promo-block">
              <div class="promo-text">About Us</div>
              <div class="center-line"></div>
            </div>
            <div class="marg50">
              <p class="about-text" align="justify"><span class="first-letter">S</span>ituated in the developing area for helping find the right path of job seekers life,
               the Life Career Services Pvt. Ltd. is highly focused on source of professional education for building careers in management.
               Basis on your qualification find jobs over in this portal , provide latest job openings surrounding you and helping to communicate to the success. 
               It represents an effective and significant investment in human potential development in India in the evolving context of the world.
                A specialized centre for professional education, the institute has been offering monthly crash courses to reduce the waiting time to build up a career.</p>
              <ul class="list-check">
                <li><a href="CouresHotel.aspx"><i class="icon-ok"></i>	<strong>Hotel Management</strong></a></li>
                </ul>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="promo-block">
              <div class="promo-text">Recent Events</div>
              <div class="center-line"></div>
            </div>
            <div class="row marg50">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ms-12">
                <div class="blog-main">
                  <div class="blog-images">
                   
                      <img src="event/careergraph%20institute%20of%20professionals_3.jpg" alt="">
                  
                  </div>
                  
                  <div class="blog-name"><a href="#">CLassRoom</a></div>
                 
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ms-12">
                <div class="blog-main">
                  <div class="blog-images">
                   
                      <img src="event/careergraph%20institute%20of%20professionals_1.jpg" alt="">
                     </div>
              
                  <div class="blog-name"><a href="#">Councelling</a></div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
	  </div> 
      <div class="container marg75">
        <div class="row">
          <div class="col-lg-12">
            <div class="promo-block">
              <div class="promo-text">Placement Companies</div>
              <div class="center-line"></div>
            </div>
          </div>
          <div class="col-lg-12 marg25">
            <div class="jcarousel-wrapper">
              <div class="jcarousel">
                <ul>
                  <%--<li><a href="#"><img src="placement/1.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/2.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/3.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/5.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/6.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/4.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/1.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/2.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/3.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/5.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/6.jpg" alt=""></a></li>
                  <li><a href="#"><img src="placement/4.jpg" alt=""></a></li>--%>
                </ul>
              </div>
              <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
              <a href="#" class="jcarousel-control-next">&rsaquo;</a>
            </div>
          </div>
        </div>
      </div>
      <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="assets/js/sticky.js"></script>
    <script type="text/javascript" src="assets/js/jquery.tweet.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/js/retina.js"></script>
    <script type="text/javascript" src="assets/js/testimonialrotator.js"></script>
    <script type="text/javascript" src="assets/js/jquery.cycle.all.js"></script>
    <script type="text/javascript" src="assets/js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="assets/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="assets/js/portfolio-main.js"></script>
    <script type="text/javascript" src="assets/js/jcarousel.responsive.js"></script>
    <script type="text/javascript" src="assets/js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dlmenu.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

    <script type="text/javascript">
        var started;
        function showLightBox() {
            if (started) return;
            started = setTimeout(function () {
                Lightbox.start(document.getElementById('firstImage'));
                started;
            }, 500);
        }
        function stopShowLightBox() {
            if (started) {
                clearTimeout(started)
                started = 0;
            }
        }
</script>

</asp:Content>
